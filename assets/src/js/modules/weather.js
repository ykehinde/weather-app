/*
WeatherAPI.js
*/

import axios from 'axios';


function doSomething() {
	console.debug('Here comes the rain!');
}

let API_KEY = "7fc4cd98d0a5d881cc5b2c0b0d00afea";
let API_UNIT_TYPE = "metric";
let API_URL = "http://api.openweathermap.org/data/2.5/weather?APPID=" + API_KEY + "&units=" + API_UNIT_TYPE;

let FORCAST_URL = "http://api.openweathermap.org/data/2.5/forecast?q=LONDON,UK&units=metric&appid=" + API_KEY;

let MESSAGE_LOCATION_NOT_FOUND = "Location not found... Try searching for something else.";
let MESSAGE_API_REQUEST_ERROR = "There was a problem getting weather data. Please try again.";
let MESSAGE_SEARCH_NOT_SPECIFIED = "Please specify a search term.";

let _waitingForData;
// _locationSearchSubmitWrapper = document.querySelector(".location-search-submit-wrapper");

let _container = document.querySelector('.forcasts');
let _forcast = document.createElement("div");
_forcast.className = "forcast";



function getData() {
	if (_waitingForData) {
		return false;
	}

	_waitingForData = false;

  var requestURL = FORCAST_URL;
  
  axios.get(FORCAST_URL)
    .then(apiRequestSuccess)
  	.catch(() => {
  		console.log(apiRequestError);
  	});
}

function forcastClass(desc) {
  var className;
  switch (desc) {
      case "clear sky":
          return className = "clear-sky";
          break;
      case "few clouds":
          return className = "few-clouds";
          break;
      case "scattered clouds":
          return className = "scattered-clouds";
          break;
      case "broken clouds":
          return className = "broken-clouds";
          break;
      case "shower rain":
          return className = "shower-rain";
          break;
      case "rain":
          return className = "rain";
          break;
      case "light rain":
          return className = "rain";
          break;
      case "thunderstorm":
          return className = "thunderstorm";
          break;
      case "snow":
          return className = "snow";
          break;
      case "mist":
          return className = "mist";
          break;
  }
}

function createForcast(date, img, desc, tempc, tempf) {
  console.log(desc);
  
  
  let _forcast = document.createElement("div");
  let _date = document.createElement("div");
  let _icon = document.createElement("div");
  let _img = document.createElement("img");
  let _desc = document.createElement("p");
  let _tempc = document.createElement("h2");
  let _tempf = document.createElement("h3");
  let _span = document.createElement("span");
  let _background = document.createElement("div");

  _forcast.classList = "forcast";
  _forcast.classList.add(forcastClass(desc));
  _date.classList = "date";
  _icon.classList = "icon";
  _desc.classList = "description"
  _tempc.classList = "tempc";
  _tempf.classList = "tempf";
  _background.classList = "background";

  _date.innerText = date.toDateString();
  _img.src = "assets/dist/img/icons/" + img + ".svg";
  _desc.innerText = desc;
  _tempc.innerText = tempc + String.fromCharCode(176) + "c";
  _tempf.innerText = tempf + String.fromCharCode(176) + "F";
  _background.style.background = "url(assets/dist/img/icons/" + img + ".svg) -70px 10px no-repeat";
  _background.style.backgroundSize = "130%";

  _forcast.appendChild(_date);
  _forcast.appendChild(_icon);
  _icon.appendChild(_img);
  _forcast.appendChild(_desc);
  _forcast.appendChild(_tempc);
  _forcast.appendChild(_tempf);
  _forcast.appendChild(_background);
  
  return _forcast;
}

function getForcast(data) {
  let dataForcast = data.data.list;

  for (let i = 0; i < dataForcast.length; i++) {    
    const element = dataForcast[i];
    let fullDate = element.dt_txt;
    let date = new Date(fullDate);
    let img = element.weather["0"].icon;
    let desc = element.weather["0"].description;
    let tempc = Math.round(element.main.temp);
    let tempf = Math.round(tempc * 9 / 5 + 32);   
    
    if (fullDate.indexOf("12:00:00") >= 0) {
      let item = createForcast(date, img, desc, tempc, tempf);
      console.log(item);
      
  
      _container.appendChild(item);
    }
  }
  // dataForcast.forEach(element => {
  //   _container.appendChild(_forcast);    
  // });
}

function apiRequestSuccess(data, xhr) {
	// console.log("weather data received", data);

	_waitingForData = false;
	// _locationSearchSubmitWrapper.classList.remove("lodaing");

	// if (typeof data.cod !== "undefined" && data.cod === "404") {
	// 	alert(MESSAGE_LOCATION_NOT_FOUND);
	// 	return;
	// }

	getForcast(data);
}

function apiRequestError(data, xhr) {
	alert(MESSAGE_API_REQUEST_ERROR);
	_waitingForData = false
}




export default function init() {
  doSomething();
  getData();
}
