/**
 * weather app
 * @description
 * @author Yemi Kehinde
 *
 * Need help using JSDoc? Find out more at http://usejsdoc.org/
 */

'use-strict';

// their code e.g. npm modules
import ready from 'lite-ready'; /* DOM ready */
import axios from 'axios'; /* https://github.com/mzabriskie/axios */
import $$ from 'double-dollar'; /* https://github.com/mrmartineau/double-dollar */
import $ from 'jquery'; /* https://jquery.com/ */

// Bundle global libs that don't return a value
import 'console';

// our code
// this is a test, uncomment the line below to try it
// import moduleTest from './modules/module-test';
import weather from './modules/weather';

window.$$ = $$;
window.jQuery = window.$ = $;

// DOM ready code goes in here
ready(() => {
	// moduleTest(); // this is a test, uncomment this line & the line above to try it
	weather();
});
